package test.inacap.a2018apr4vgholamundo.controlador;

import android.content.ContentValues;
import android.content.Context;

import test.inacap.a2018apr4vgholamundo.modelo.MainDBContract;
import test.inacap.a2018apr4vgholamundo.modelo.UsuariosModel;

public class UsuariosController {
    private UsuariosModel capaModelo;

    public UsuariosController(Context context){
        this.capaModelo = new UsuariosModel(context);
    }

    public void crearUsuario(String userName, String password1, String password2) throws Exception{
        // Comprar las password
        if(!password1.equals(password2)){
            // Lanzar una Exception
            // esta Exception debe ser manejada por la capa vista
            throw new Exception("Las contraseñas no coinciden :-(");
        }

        // Guardar los datos
        ContentValues usuario = new ContentValues();
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, userName);
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, password1);

        this.capaModelo.crearUsuario(usuario);
    }

    public int login(String username, String password){
        // Si los datos son correctos devolvemos 0
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorUsername(username);

        // Validar que el usuario exista
        if(usuario == null){
            // NO existe
            return 2;
        }

        // Validar password
        String passDB = usuario.getAsString(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD);
        if(passDB.equals(password)){
            // ok
            return 0;
        }

        return 1;

    }
}

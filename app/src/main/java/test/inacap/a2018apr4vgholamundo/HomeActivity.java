package test.inacap.a2018apr4vgholamundo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button btSalir = (Button) findViewById(R.id.btSalir);

        btSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                SharedPreferences.Editor edi = sesiones.edit();
                edi.putBoolean("inicioSesion", false);
                edi.commit();

                Intent i = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}

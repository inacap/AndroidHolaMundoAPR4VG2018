package test.inacap.a2018apr4vgholamundo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import test.inacap.a2018apr4vgholamundo.controlador.UsuariosController;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegistrar;
    private TextView tvMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword1 = (EditText) findViewById(R.id.etPassword1);
        etPassword2 = (EditText) findViewById(R.id.etPassword2);

        btRegistrar = (Button) findViewById(R.id.btRegistrar);

        tvMensaje = (TextView) findViewById(R.id.tvMensaje);

        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = etUsername.getText().toString();
                String password1 = etPassword1.getText().toString().trim();
                String password2 = etPassword2.getText().toString().trim();

                UsuariosController capaControlador = new UsuariosController(getApplicationContext());
                try {
                    capaControlador.crearUsuario(userName, password1, password2);
                } catch (Exception e){
                    String mensaje = e.getMessage();
                    tvMensaje.setText(mensaje);
                }

            }
        });

    }
}

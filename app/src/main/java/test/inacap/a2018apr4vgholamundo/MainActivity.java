package test.inacap.a2018apr4vgholamundo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.a2018apr4vgholamundo.controlador.UsuariosController;

public class MainActivity extends AppCompatActivity {

    public EditText etNombreUsuario, etPass;
    public Button btIngresar;
    public TextView tvMensaje, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Comprobar si la persona ya habia iniciado sesion
        SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesiones.getBoolean("inicioSesion", false);

        if(yaInicioSesion){
            Intent intent = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(intent);
            finish();
        }

        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etPass = findViewById(R.id.etContrasena);
        this.btIngresar  = findViewById(R.id.btIngresar);

        this.tvMensaje = findViewById(R.id.tvMensaje);
        this.tvRegistrar = findViewById(R.id.tvRegistrar);

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent
                Intent intentNuevaVentana = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(intentNuevaVentana);
            }
        });

        this.btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Capturar y mostrar los datos
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPass.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                int inicioSesion = controller.login(nombreUsuario, password);

                if(inicioSesion == 0){
                    // Inicio sesion exitosamente
                    // SharedPreferences
                    SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();

                    editor.putBoolean("inicioSesion", true);
                    editor.commit();

                    Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}











package test.inacap.a2018apr4vgholamundo.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModel {
    private MainDBHelper dbHelper;

    public UsuariosModel(Context c){
        this.dbHelper = new MainDBHelper(c);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(MainDBContract.MainDBUsuarios.NOMBRE_TABLA, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username){
        // Abrir conexion
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // SELECT * FROM usuarios WHERE username = ? LIMIT 1

        // Projection
        String[] projection = {
                MainDBContract.MainDBUsuarios._ID,
                MainDBContract.MainDBUsuarios.COLUMNA_USERNAME,
                MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD
        };

        // Clausulas
        String selection = MainDBContract.MainDBUsuarios.COLUMNA_USERNAME + " = ? LIMIT 1";
        // Valores
        String[] selectionArgs = { username };

        Cursor cursor = db.query(
                MainDBContract.MainDBUsuarios.NOMBRE_TABLA,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );

        // ver si existe un usuario

        if(cursor.moveToFirst()){
            // el usuario si existe

            // preparamos los datos

            String username_cursor = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME));
            String password_cursor = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, username_cursor);
            usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, password_cursor);

            return usuario;
        }

        return null;
    }
}
